package com.demo.accountmanagement.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class MoneyTransferRequest {

    @NotNull(message = "Source account number cannot be null")
    private Long sourceAccountNumber;

    @NotNull(message = "Destination account number cannot be null")
    private Long destinationAccountNumber;

    @NotNull(message = "Amount cannot be null")
    private BigDecimal amount;

    public MoneyTransferRequest() {
    }

    public MoneyTransferRequest(Long sourceAccountNumber, Long destinationAccountNumber, BigDecimal amount) {
        this.sourceAccountNumber = sourceAccountNumber;
        this.destinationAccountNumber = destinationAccountNumber;
        this.amount = amount;
    }

    public Long getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public void setSourceAccountNumber(Long sourceAccountNumber) {
        this.sourceAccountNumber = sourceAccountNumber;
    }

    public Long getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    public void setDestinationAccountNumber(Long destinationAccountNumber) {
        this.destinationAccountNumber = destinationAccountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
