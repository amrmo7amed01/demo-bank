package com.demo.accountmanagement.controller;

import com.demo.accountmanagement.dto.AccountDetailsResponse;
import com.demo.accountmanagement.dto.MoneyTransferRequest;
import com.demo.accountmanagement.service.AccountManagementService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping("${app.config.base-uri}")
public class AccountManagementController {

    @Autowired
    private AccountManagementService accountManagementService;

    @Operation(summary = "Allow user to transfer money from one of his accounts to another user account")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Successful transfer"),
            @ApiResponse(responseCode = "400", description = "Invalid Input"),
            @ApiResponse(responseCode = "422", description = "Transfer could not be fulfilled")})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(value = "${app.config.transfer-money-uri}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void makeMoneyTransfer(@Valid @RequestBody MoneyTransferRequest transferRequest) {

        accountManagementService.makeMoneyTransfer(transferRequest);
    }

    @Operation(summary = "Allow user to retrieve details of his account")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Account details successfully retrieved"),
            @ApiResponse(responseCode = "400", description = "Invalid Input"),
            @ApiResponse(responseCode = "422", description = "Account does not exist")})
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "${app.config.retrieve-account-details-uri}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public AccountDetailsResponse retrieveAccountDetails(
            @PathVariable("account-number") long accountNumber) {

        return accountManagementService.retrieveAccountDetails(accountNumber);
    }

}
