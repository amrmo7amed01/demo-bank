package com.demo.accountmanagement.controller;

import com.demo.accountmanagement.dto.AccountDetailsResponse;
import com.demo.accountmanagement.dto.MoneyTransferRequest;
import com.demo.accountmanagement.entity.Account;
import com.demo.accountmanagement.exception.ErrorResponse;
import com.demo.accountmanagement.repository.AccountsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
public class AccountManagementControllerTests {

    @Value("${app.config.base-uri}")
    private String baseUri;

    @Value("${app.config.transfer-money-uri}")
    private String transferMoneyUri;

    @Value("${app.config.retrieve-account-details-uri}")
    private String retrieveAccountDetailsUri;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AccountsRepository accountsRepository;

    private static Stream<Arguments> provideMakeMoneyTransferInvalidDtoParams() {

        return Stream.of(Arguments.of(new MoneyTransferRequest(null, 12345L, BigDecimal.valueOf(1000))),
                Arguments.of(new MoneyTransferRequest(98765L, null, BigDecimal.valueOf(1000))),
                Arguments.of(new MoneyTransferRequest(98765L, 12345L, null)));
    }

    @ParameterizedTest
    @MethodSource("provideMakeMoneyTransferInvalidDtoParams")
    void whenCreatingCustomerWithInvalidParameters(MoneyTransferRequest moneyTransferRequest) throws Exception {

        mockMvc.perform(post(baseUri + transferMoneyUri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(moneyTransferRequest)))
                .andDo(result -> {
                    Assertions.assertThat(result.getResponse().getStatus())
                            .isEqualTo(HttpStatus.BAD_REQUEST.value());
                });
    }

    @Test
    void makeMoneyTransfer_withValidInput_thenTransactionReflectedSuccessfully() throws Exception {

        MoneyTransferRequest moneyTransferRequest = new MoneyTransferRequest();
        moneyTransferRequest.setSourceAccountNumber(12345678888L);
        moneyTransferRequest.setDestinationAccountNumber(12345678921L);
        moneyTransferRequest.setAmount(BigDecimal.valueOf(20000));

        mockMvc.perform(post(baseUri + transferMoneyUri)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(moneyTransferRequest)))
                .andDo(result -> {
                    Account sourceAccount =
                            accountsRepository.findByAccountNumber(
                                    moneyTransferRequest.getSourceAccountNumber()).get();
                    Account destinationAccount =
                            accountsRepository.findByAccountNumber(
                                    moneyTransferRequest.getDestinationAccountNumber()).get();

                    Assertions.assertThat(result.getResponse().getStatus())
                    .isEqualTo(HttpStatus.NO_CONTENT.value());
                    Assertions.assertThat(sourceAccount.getBalance()
                            .compareTo(BigDecimal.valueOf(1980000))).isEqualTo(0);
                    Assertions.assertThat(destinationAccount.getBalance()
                            .compareTo(BigDecimal.valueOf(30000))).isEqualTo(0);
        });
    }

    @Test
    void makeMoneyTransfer_withWrongSourceAccountNumber_thenSourceDoesNotExist() throws Exception {

        MoneyTransferRequest moneyTransferRequest = new MoneyTransferRequest();
        moneyTransferRequest.setSourceAccountNumber(1122233L);
        moneyTransferRequest.setDestinationAccountNumber(12345678921L);
        moneyTransferRequest.setAmount(BigDecimal.valueOf(20000));

        mockMvc.perform(post(baseUri + transferMoneyUri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(moneyTransferRequest)))
                .andDo(result -> {

                    ErrorResponse errorResponse = objectMapper.
                            readValue(result.getResponse().getContentAsByteArray(), ErrorResponse.class);

                    Assertions.assertThat(result.getResponse().getStatus())
                            .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());

                    Assertions.assertThat(errorResponse.getCode())
                            .isEqualTo(1002);
                });
    }

    @Test
    void makeMoneyTransfer_withNegativeMoneyAmount_thenAmountCanNotBeNegative() throws Exception {

        MoneyTransferRequest moneyTransferRequest = new MoneyTransferRequest();
        moneyTransferRequest.setSourceAccountNumber(12345678888L);
        moneyTransferRequest.setDestinationAccountNumber(12345678921L);
        moneyTransferRequest.setAmount(BigDecimal.valueOf(-20000));

        mockMvc.perform(post(baseUri + transferMoneyUri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(moneyTransferRequest)))
                .andDo(result -> {

                    ErrorResponse errorResponse = objectMapper.
                            readValue(result.getResponse().getContentAsByteArray(), ErrorResponse.class);

                    Assertions.assertThat(result.getResponse().getStatus())
                            .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());

                    Assertions.assertThat(errorResponse.getCode())
                            .isEqualTo(1001);
                });
    }

    @Test
    void makeMoneyTransfer_withBlockedUser_thenBlockedUser() throws Exception {

        MoneyTransferRequest moneyTransferRequest = new MoneyTransferRequest();
        moneyTransferRequest.setSourceAccountNumber(12345678912L);
        moneyTransferRequest.setDestinationAccountNumber(12345678921L);
        moneyTransferRequest.setAmount(BigDecimal.valueOf(20000));

        mockMvc.perform(post(baseUri + transferMoneyUri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(moneyTransferRequest)))
                .andDo(result -> {

                    ErrorResponse errorResponse = objectMapper.
                            readValue(result.getResponse().getContentAsByteArray(), ErrorResponse.class);

                    Assertions.assertThat(result.getResponse().getStatus())
                            .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());

                    Assertions.assertThat(errorResponse.getCode())
                            .isEqualTo(1003);
                });
    }

    @Test
    void makeMoneyTransfer_withBlockedAccount_thenBlockedAccount() throws Exception {

        MoneyTransferRequest moneyTransferRequest = new MoneyTransferRequest();
        moneyTransferRequest.setSourceAccountNumber(12345674444L);
        moneyTransferRequest.setDestinationAccountNumber(12345678921L);
        moneyTransferRequest.setAmount(BigDecimal.valueOf(20000));

        mockMvc.perform(post(baseUri + transferMoneyUri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(moneyTransferRequest)))
                .andDo(result -> {

                    ErrorResponse errorResponse = objectMapper.
                            readValue(result.getResponse().getContentAsByteArray(), ErrorResponse.class);

                    Assertions.assertThat(result.getResponse().getStatus())
                            .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());

                    Assertions.assertThat(errorResponse.getCode())
                            .isEqualTo(1004);
                });
    }

    @Test
    void makeMoneyTransfer_withWrongDestinationAccountNumber_thenDestinationAccountDoesNotExist() throws Exception {

        MoneyTransferRequest moneyTransferRequest = new MoneyTransferRequest();
        moneyTransferRequest.setSourceAccountNumber(12345678888L);
        moneyTransferRequest.setDestinationAccountNumber(12345647231L);
        moneyTransferRequest.setAmount(BigDecimal.valueOf(20000));

        mockMvc.perform(post(baseUri + transferMoneyUri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(moneyTransferRequest)))
                .andDo(result -> {
                    ErrorResponse errorResponse = objectMapper.
                            readValue(result.getResponse().getContentAsByteArray(), ErrorResponse.class);

                    Assertions.assertThat(result.getResponse().getStatus())
                            .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());

                    Assertions.assertThat(errorResponse.getCode())
                            .isEqualTo(1005);

                    Optional<Account> sourceAccount = accountsRepository
                            .findByAccountNumber(12345678888L);
                    Assertions.assertThat(sourceAccount.get().getBalance()
                                    .compareTo(BigDecimal.valueOf(2000000))).isEqualTo(0);
                });
    }

    @Test
    void makeMoneyTransfer_withExceedingLimitAmount_thenNoSufficientBalance() throws Exception {

        MoneyTransferRequest moneyTransferRequest = new MoneyTransferRequest();
        moneyTransferRequest.setSourceAccountNumber(12345673333L);
        moneyTransferRequest.setDestinationAccountNumber(12345678921L);
        moneyTransferRequest.setAmount(BigDecimal.valueOf(20000));

        mockMvc.perform(post(baseUri + transferMoneyUri)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(moneyTransferRequest)))
                .andDo(result -> {

                    ErrorResponse errorResponse = objectMapper.
                            readValue(result.getResponse().getContentAsByteArray(), ErrorResponse.class);

                    Assertions.assertThat(result.getResponse().getStatus())
                            .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());

                    Assertions.assertThat(errorResponse.getCode())
                            .isEqualTo(1006);
                });
    }

    @Test
    void retrieveAccountDetails_withValidInput_thenAccountDetailsRetrievedSuccessfully()
            throws Exception {

        Long accountNumber = 12345678921L;

        String url = UriComponentsBuilder
                .fromUriString(baseUri + retrieveAccountDetailsUri).buildAndExpand(accountNumber)
                .toUriString();

        mockMvc.perform(get(url).accept(MediaType.APPLICATION_JSON)).andDo(result -> {

            AccountDetailsResponse accountDetailsResponse = objectMapper.
                    readValue(result.getResponse().getContentAsByteArray(),
                            AccountDetailsResponse.class);

            Account account = accountsRepository
                    .findByAccountNumber(accountNumber).get();

            Assertions.assertThat(result.getResponse().getStatus())
                    .isEqualTo(HttpStatus.OK.value());

            Assertions.assertThat(account.getAccountNumber())
                    .isEqualTo(accountDetailsResponse.getAccountNumber());
            Assertions.assertThat(account.getBalance())
                    .isEqualTo(accountDetailsResponse.getBalance());
            Assertions.assertThat(account.getStatus())
                    .isEqualTo(accountDetailsResponse.getStatus());
            Assertions.assertThat(account.getUser().getId())
                    .isEqualTo(accountDetailsResponse.getOwnerId());
        });
    }

    @Test
    void retrieveAccountDetails_withNonExistentAccount_thenAccountDoesNotExist()
            throws Exception {

        Long accountNumber = 98237463L;

        String url = UriComponentsBuilder
                .fromUriString(baseUri + retrieveAccountDetailsUri).buildAndExpand(accountNumber)
                .toUriString();

        mockMvc.perform(get(url).accept(MediaType.APPLICATION_JSON)).andDo(result -> {

            ErrorResponse errorResponse = objectMapper.
                    readValue(result.getResponse().getContentAsByteArray(), ErrorResponse.class);

            Assertions.assertThat(result.getResponse().getStatus())
                    .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());

            Assertions.assertThat(errorResponse.getCode())
                    .isEqualTo(1007);
        });
    }

}

