package com.demo.accountmanagement.mapper;

import com.demo.accountmanagement.dto.AccountDetailsResponse;
import com.demo.accountmanagement.entity.Account;
import org.springframework.stereotype.Component;

@Component
public class AccountMapper {

    public AccountDetailsResponse mapToDto(Account account) {

        AccountDetailsResponse accountDetailsResponse = new AccountDetailsResponse();

        accountDetailsResponse.setAccountNumber(account.getAccountNumber());
        accountDetailsResponse.setBalance(account.getBalance());
        accountDetailsResponse.setStatus(account.getStatus());
        accountDetailsResponse.setOwnerId(account.getUser().getId());

        return accountDetailsResponse;
    }

}
