package com.demo.accountmanagement.exception;

public class ApiException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	protected int code;

	public ApiException() {
		super();
	}

	public ApiException(String message) {
		super(message);
	}

	public ApiException(String message, int code) {
		super(message);
		this.code = code;
	}

	public ApiException(String message, Throwable cause) {
		super(message, cause);
	}

	public ApiException(Throwable cause) {
		super(cause);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}
