package com.demo.accountmanagement.dto;

import com.demo.accountmanagement.constant.Status;

import java.math.BigDecimal;

public class AccountDetailsResponse {

    private long accountNumber;

    private BigDecimal balance;

    private Status status;

    private long ownerId;

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

}
