MERGE INTO `users` (id, first_name, last_name, national_id, status)
VALUES (1, 'Amr', 'Saad', '12345678912345', 'ACTIVE');

MERGE INTO `users` (id, first_name, last_name, national_id, status)
VALUES (2, 'Kholoud', 'Saad', '12345678954321', 'BLOCKED');

MERGE INTO `users` (id, first_name, last_name, national_id, status)
VALUES (3, 'Fatma', 'Sharawy', '12345678998765', 'ACTIVE');

MERGE INTO `accounts` (id, account_number, balance, status, user_id)
VALUES (1, 12345678912, 15000, 'ACTIVE', 2) ;

MERGE INTO `accounts` (id, account_number, balance, status, user_id)
VALUES (2, 12345678921, 10000, 'ACTIVE', 1) ;

MERGE INTO `accounts` (id, account_number, balance, status, user_id)
VALUES (3, 12345678888, 2000000, 'ACTIVE', 3);

MERGE INTO `accounts` (id, account_number, balance, status, user_id)
VALUES (4, 12345674444, 50000, 'BLOCKED', 3);

MERGE INTO `accounts` (id, account_number, balance, status, user_id)
VALUES (5, 12345673333, 1000, 'ACTIVE', 3) ;