package com.demo.accountmanagement.service.impl;

import com.demo.accountmanagement.constant.Status;
import com.demo.accountmanagement.dto.AccountDetailsResponse;
import com.demo.accountmanagement.dto.MoneyTransferRequest;
import com.demo.accountmanagement.entity.Account;
import com.demo.accountmanagement.exception.ApiException;
import com.demo.accountmanagement.mapper.AccountMapper;
import com.demo.accountmanagement.repository.AccountsRepository;
import com.demo.accountmanagement.service.AccountManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
public class AccountManagementServiceImpl implements AccountManagementService {

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private AccountsRepository accountsRepository;

    @Override
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void makeMoneyTransfer(MoneyTransferRequest moneyTransferRequest) {

        if (moneyTransferRequest.getAmount().compareTo(BigDecimal.valueOf(0)) < 0)
            throw new ApiException("Money amount cannot be negative", 1001);

        Account sourceAccount = accountsRepository.findByAccountNumber(
                moneyTransferRequest.getSourceAccountNumber()).orElseThrow(() ->
                new ApiException("Source account does not exist", 1002));

        if (!Status.ACTIVE.equals(sourceAccount.getUser().getStatus()))
            throw new ApiException("User is blocked, cannot process request", 1003);

        if (!Status.ACTIVE.equals(sourceAccount.getStatus()))
            throw new ApiException("Account is blocked, cannot process request", 1004);

        if (sourceAccount.getBalance().compareTo(moneyTransferRequest.getAmount()) < 0)
            throw new ApiException("No sufficient balance for the transaction", 1006);

        BigDecimal sourceAccountNewBalance = sourceAccount.getBalance()
                .subtract(moneyTransferRequest.getAmount());
        sourceAccount.setBalance(sourceAccountNewBalance);

        accountsRepository.save(sourceAccount);

        Account destinationAccount = accountsRepository.findByAccountNumber(
                moneyTransferRequest.getDestinationAccountNumber()).orElseThrow(() ->
                new ApiException("Destination account does not exist", 1005));

        BigDecimal destinationAccountNewBalance = destinationAccount.getBalance()
                .add(moneyTransferRequest.getAmount());
        destinationAccount.setBalance(destinationAccountNewBalance);

        accountsRepository.save(destinationAccount);
    }

    @Override
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public AccountDetailsResponse retrieveAccountDetails(long accountNumber) {

        Account account = accountsRepository.findByAccountNumber(accountNumber).orElseThrow(() ->
                new ApiException("Account does not exist", 1007));

        return accountMapper.mapToDto(account);
    }

}
