package com.demo.accountmanagement.service;

import com.demo.accountmanagement.dto.AccountDetailsResponse;
import com.demo.accountmanagement.dto.MoneyTransferRequest;

public interface AccountManagementService {

    void makeMoneyTransfer(MoneyTransferRequest moneyTransferRequest);

    AccountDetailsResponse retrieveAccountDetails(long accountNumber);

}
