FROM openjdk:8-jdk-alpine
MAINTAINER baeldung.com
COPY target/account-management-0.0.1.jar account-management-0.0.1.jar
ENTRYPOINT ["java","-jar","/account-management-0.0.1.jar"]