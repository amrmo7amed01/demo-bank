package com.demo.accountmanagement.repository;

import com.demo.accountmanagement.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, Long> {
}
