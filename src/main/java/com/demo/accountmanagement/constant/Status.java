package com.demo.accountmanagement.constant;

public enum Status {
    ACTIVE, BLOCKED
}
